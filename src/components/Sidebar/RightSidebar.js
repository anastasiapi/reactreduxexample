import Aside from './Sidebar'
import React from 'react'
import Status from '../Status/Status'

/*
 * Right sidebar with widgets
 */
function RightSidebar() {
    return (
        <Aside color="grey">
            <Status />
        </Aside>
    )
}

export default RightSidebar
