import React from 'react'
import { useSelector } from 'react-redux'

/*
 * Widget to display current exchange rate
 */
function Exchange() {
    const { price$ } = useSelector((state) => state.bitcoin)

    return <div className="Header__Exchange">1 bitcoin = {price$}</div>
}

export default Exchange
