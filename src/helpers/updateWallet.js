import { updateHistory } from '../store/history/history.slice'

// Dispatch a set of actions to update the Wallet and History
// @action any redux action
// @amount money or bitcoins: number
// @display money with curr sign: string
// @dispatch useDispatch(): fn
export default function (action, dispatch, amount, display) {
    const value = display || amount

    dispatch(action(amount))
    dispatch(updateHistory({ type: `${action}`, amount: value }))
}
