import React from 'react'
import { NavLink } from 'react-router-dom'
import logo from '../../assets/images/bz-logo.png'
import { APP_NAME } from '../../config/app.config'
import './Meta.scss'

/*
 * App meta for displaying branding
 */
function Meta() {
    return (<div className="Header__Logo">
        <NavLink to="/wallet" className="Logo__Link">
            <img src={logo} alt="Bitcoin Frenzy Logo" />
            <span className="Title">{APP_NAME}</span>
        </NavLink>
    </div>)

}

export default Meta
