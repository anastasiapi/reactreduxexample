import Aside from './Sidebar'
import React from 'react'
import Navigation from '../Navigation/Navigation'

/*
 * Left sidebar with widgets
 */
function LeftSidebar() {
    return (
        <Aside color="blue">
            <Navigation />
        </Aside>
    )
}

export default LeftSidebar
