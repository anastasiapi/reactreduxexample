import React from 'react'
import { useSelector } from 'react-redux'

/*
 * Widget to display current wallet state
 */
function WalletState() {
    const { bitcoin, fiat$ } = useSelector((state) => state.wallet)

    return (
        <div className="Header__Wallet">
            {fiat$} <br />
            {bitcoin} bitcoins
        </div>
    )
}

export default WalletState
