// Assuming wallet data comes from backend
// @send http get req using axios as example
// @dispatch data to the store
import { getWalletSuccess, getWalletError } from './wallet.slice'
import { INIT_FIAT } from '../../config/app.config'

export function fetchWalletData() {
    return async (dispatch) => {
        try {
            // const response = await http.get('/wallet')
            /* dispatch the data from request getWalletSuccess(response.data) */
            dispatch(
                getWalletSuccess({
                    bitcoin: 8,
                    fiat: INIT_FIAT,
                    curr: 'usd',
                }),
            )
            // return response
        } catch (e) {
            dispatch(getWalletError())
            console.log(`GET request failed: ${e}`)
            throw e
        }
    }
}
