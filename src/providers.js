import { Provider } from 'react-redux'
import PropTypes from 'prop-types'
import store from './store/store'
import { BrowserRouter as Router } from 'react-router-dom'
import React from 'react'

/*
 * App providers
 * Usage - app and testing
 */
function Providers({ children }) {
    return (
        <Router>
            <Provider store={store}>{children}</Provider>
        </Router>
    )
}

Providers.propTypes = {
    children: PropTypes.object.isRequired,
}

export default Providers
