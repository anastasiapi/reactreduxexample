import { NavLink } from 'react-router-dom'
import Icon from '../Icon/Icon'
import React from 'react'
import PropTypes from 'prop-types'

function NavigationItem({ link }) {
    return (
        <li>
            <NavLink className="Navigation__Link" activeClassName="selected" to={`${link.path}`}>
                <span className="Icon">
                    <Icon />
                </span>

                {link.label}
            </NavLink>
        </li>
    )
}

NavigationItem.propTypes = {
    link: PropTypes.object.isRequired,
}

export default NavigationItem
