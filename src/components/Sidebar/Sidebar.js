import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const StyledAside = styled.aside`
    background-color: ${props => props.color === 'grey' ? '#d8d8d8' : '#394577'};
    color: ${props => props.color === 'grey' ? '#29335c' : '#ced4eb'};
    padding: 0;
    overflow-y: auto;
    height: 100vh;
    min-width: 240px;
`

/*
 * Common Aside component
 */
function Aside(props) {
    const { children, color, className } = props

    return <StyledAside className={className} color={color}>{children}</StyledAside>
}

Aside.propTypes = {
    children: PropTypes.object.isRequired,
    color: PropTypes.string.isRequired,
    className: PropTypes.string.isRequired,
}

export default Aside
