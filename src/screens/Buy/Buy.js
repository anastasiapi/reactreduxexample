import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { useDispatch, useSelector } from 'react-redux'
import { purchased, withdraw } from '../../store/wallet/wallet.slice'
import updateWallet from '../../helpers/updateWallet'
import initialState from '../../store/initialState'

// Get error message
function getErrorText(data) {
    return `Not enough money. You need ${data}$ more. Deposit more or sell some bitcoins`
}

/*
 * Buy Bitcoin Page
 */
function Buy({ bitcoinPrice, isHigh, title, price$ }) {
    // Redux hooks
    const dispatch = useDispatch()
    const { fiat } = useSelector((state) => state.wallet)
    const { minBuyBitcoin } = useSelector((state) => state.settings)
    const [errorMsg, setErrorMsg] = useState('')

    // Validate current wallet state, process errors and dispatch relevant redux actions
    function handleClick() {
        if (fiat < bitcoinPrice) {
            setErrorMsg(getErrorText(bitcoinPrice - fiat))
        } else {
            updateWallet(purchased, dispatch, minBuyBitcoin)
            dispatch(withdraw(minBuyBitcoin * bitcoinPrice))
            setErrorMsg('')
        }
    }

    return (
        <>
            <h2 className="Page__Title">Bitcoin price is {price$}</h2>

            {isHigh && <h1 className="Page__Subtitle">Prices are high, are you sure that you want to buy?</h1>}
            {!isHigh && <h1 className="Page__Subtitle">Prices are low, buy more!</h1>}

            <div className="Buttons__Container">
                <button disabled={!!errorMsg} onClick={handleClick}>
                    Buy {minBuyBitcoin} {title}
                </button>
            </div>

            {errorMsg && <div className="Error">{errorMsg}</div>}
        </>
    )
}

Buy.defaultProps = {
    bitcoinPrice: initialState.bitcoinPrice,
    price$: '',
}

Buy.propTypes = {
    bitcoinPrice: PropTypes.number,
    isHigh: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    price$: PropTypes.string,
}

export default Buy
