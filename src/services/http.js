import axios from 'axios'

/*
 * Axios instance example
 */
export const http = axios.create({
    baseURL: 'http://localhost:3000',
    headers: {
        Accept: 'application/json, text/plain, */*',
    },
})
