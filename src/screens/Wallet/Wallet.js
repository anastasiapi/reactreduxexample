import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { deposit, withdraw } from '../../store/wallet/wallet.slice'
import updateWallet from '../../helpers/updateWallet'

/*
 * Wallet Page
 */
function Wallet() {
    // Redux hooks
    const dispatch = useDispatch()
    const { bitcoin, fiat } = useSelector((state) => state.wallet)
    const { maxDeposit, maxDeposit$, maxWithdrawal, maxWithdrawal$ } = useSelector((state) => state.settings)

    return (
        <>
            <h2 className="Page__Title">Your bitcoin wallet</h2>
            <h1 className="Page__Subtitle">You now own {bitcoin} Bitcoins</h1>

            <div className="Buttons__Container">
                <button onClick={() => updateWallet(deposit, dispatch, maxDeposit, maxDeposit$)}>
                    Deposit {maxDeposit$}
                </button>
                <button
                    onClick={() => updateWallet(withdraw, dispatch, maxWithdrawal, maxWithdrawal$)}
                    disabled={fiat === 0}
                >
                    Withdraw {maxWithdrawal$}
                </button>
            </div>
        </>
    )
}

export default Wallet
