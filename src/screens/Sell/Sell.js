import React from 'react'
import PropTypes from 'prop-types'
import { useDispatch, useSelector } from 'react-redux'
import updateWallet from '../../helpers/updateWallet'
import { deposit, sold } from '../../store/wallet/wallet.slice'
import initialState from '../../store/initialState'

/*
 * Sell Bitcoin Page
 */
function Sell({ bitcoinPrice, isHigh, title, price$ }) {
    // Redux hooks
    const dispatch = useDispatch()
    const { minBuyBitcoin } = useSelector((state) => state.settings)

    // Dispatch redux actions on btn click
    function handleClick() {
        updateWallet(sold, dispatch, minBuyBitcoin)
        dispatch(deposit(minBuyBitcoin * bitcoinPrice))
    }

    return (
        <>
            <h2 className="Page__Title">Bitcoin price is {price$}</h2>

            {isHigh && <h1 className="Page__Subtitle">Prices are high, sell now!</h1>}
            {!isHigh && <h1 className="Page__Subtitle">Prices are low, are you sure you want to sell?</h1>}

            <div className="Buttons__Container">
                <button onClick={handleClick}>
                    Sell {minBuyBitcoin} {title}
                </button>
            </div>
        </>
    )
}

Sell.defaultProps = {
    bitcoinPrice: initialState.bitcoinPrice,
    price$: '',
}

Sell.propTypes = {
    bitcoinPrice: PropTypes.number,
    isHigh: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    price$: PropTypes.string,
}

export default Sell
