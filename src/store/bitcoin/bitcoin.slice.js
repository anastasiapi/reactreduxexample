import initialState from '../initialState'
import { createSlice } from '@reduxjs/toolkit'
import format from '../../helpers/formatAmount'

const bitcoinSlice = createSlice({
    name: 'bitcoin',
    initialState: initialState.bitcoinPrice,
    reducers: {
        getPriceSuccess: (state, action) => ({
            ...action.payload,
            price$: format(action.payload.price, action.payload.curr),
        }),
        getPriceError: (state) => state,
        increased: (state, action) => ({
            ...state,
            price: state.price + action.payload,
            price$: format(state.price + action.payload, state.curr),
        }),
        decreased: (state, action) => ({
            ...state,
            price: state.price - action.payload,
            price$: format(state.price - action.payload, state.curr),
        }),
    },
})

export const { getPriceSuccess, getPriceError, increased, decreased } = bitcoinSlice.actions

export default bitcoinSlice.reducer
