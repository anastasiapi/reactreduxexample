/*
 * Redux store initial state
 */
export default {
    wallet: {
        bitcoin: 0,
        fiat: 0,
    },
    bitcoinPrice: 0,
    settings: {
        maxDeposit: 0,
        maxWithdrawal: 0,
        initFiat: 0,
        maxBuyBitcoin: 0,
        highPrice: 0,
        lowPrice: 0,
        bitcoinStep: 0,
        bitcoinInitPrice: 0,
        userCurrency: 'usd',
    },
    history: [],
}
