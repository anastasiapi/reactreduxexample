import React from 'react'
import './status.scss'
import { useSelector } from 'react-redux'
import HistoryEvent from './HistoryEvent'

/*
 * Sidebar with system status data
 */
function Status() {
    const history = useSelector((state) => state.history)
    const isEmpty = history.length === 0

    return (
        <div className="History__Actions">
            <h3>Recent history</h3>
            {isEmpty ? (
                <h4>
                    Nothing here yet. <br />
                    Start selling and buying
                </h4>
            ) : (
                history.map((item) => <HistoryEvent key={item.id} item={item} />)
            )}
        </div>
    )
}

export default Status
