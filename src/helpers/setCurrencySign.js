// Returns a sign for provided currency
// @currency short: string
export default (currency) => ({
    'eur': '€',
    'usd': '$',
})[currency]
