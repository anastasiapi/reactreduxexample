import React from 'react'
import './navigation.scss'
import NavigationItem from './NavigationItem'
import { routes } from '../../config/app.config'

/*
 * App navigation
 */
function Navigation() {
    return (
        <nav className="Navigation">
            <ul>{routes.map((link) => (!link.exclude ? <NavigationItem link={link} key={link.id} /> : null))}</ul>
        </nav>
    )
}

export default Navigation
