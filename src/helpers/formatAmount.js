import setCurrencySign from './setCurrencySign'

// Create string literal - amount and currency sign
// @amount sum: number
// @curr currency: string
export default (amount, curr) => `${amount}${setCurrencySign(curr)}`
