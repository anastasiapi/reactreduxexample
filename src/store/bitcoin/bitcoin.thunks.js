// Assuming that Bitcoin price comes from backend
// @send http get req using axios as example
// @dispatch data to the store
import { getPriceError, getPriceSuccess } from './bitcoin.slice'

export function fetchBitcoinPrice() {
    return async (dispatch) => {
        try {
            // const response = await http.get('/price')
            dispatch(getPriceSuccess({ price: 10000, curr: 'usd' }))
            // return response
        } catch (e) {
            dispatch(getPriceError())
            console.log(`GET request failed: ${e}`)
            throw e
        }
    }
}
