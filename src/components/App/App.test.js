import React from 'react'
import { render, screen } from '@testing-library/react'
import App from './App'
import Providers from '../../providers'

test('renders learn react link', () => {
    render(
        <Providers>
            <App />
        </Providers>,
    )
    const linkElement = screen.getByText(/bitcoin/i)
    expect(linkElement).toBeInTheDocument()
})
