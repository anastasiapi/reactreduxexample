import React from 'react'
import './header.scss'
import Meta from '../Meta/Meta'
import Exchange from '../Exchange/Exchange'
import WalletState from '../WalletState/WalletState'

/*
 * App main header
 */
function Header() {
    return (
        <header className="Header">
            <Meta />
            <Exchange />
            <WalletState />
        </header>
    )
}

export default Header
