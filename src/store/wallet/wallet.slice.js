import initialState from '../initialState'
import { createSlice } from '@reduxjs/toolkit'
import format from '../../helpers/formatAmount'

const walletSlice = createSlice({
    name: 'wallet',
    initialState: initialState.wallet,
    reducers: {
        getWalletSuccess: (state, action) => ({
            ...action.payload,
            fiat$: format(action.payload.fiat, action.payload.curr),
        }),
        getWalletError: (state) => state,
        deposit: (state, action) => ({
            ...state,
            fiat: state.fiat + action.payload,
            fiat$: format(state.fiat + action.payload, state.curr),
        }),
        withdraw: (state, action) => ({
            ...state,
            fiat: state.fiat - action.payload,
            fiat$: format(state.fiat - action.payload, state.curr),
        }),
        purchased: (state, action) => ({ ...state, bitcoin: state.bitcoin + action.payload }),
        sold: (state, action) => ({ ...state, bitcoin: state.bitcoin - action.payload }),
    },
})

export const { getWalletSuccess, getWalletError, deposit, withdraw, purchased, sold } = walletSlice.actions

export default walletSlice.reducer
