import React from 'react'

/*
 * 404 Page
 */
function NotFound() {
    return <h1>Page Not Found</h1>
}

export default NotFound
