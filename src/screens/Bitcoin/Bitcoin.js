import React from 'react'
import PropTypes from 'prop-types'
import { useDispatch, useSelector } from 'react-redux'
import updateWallet from '../../helpers/updateWallet'
import { decreased, increased } from '../../store/bitcoin/bitcoin.slice'

/*
 * Bitcoin Page
 */
function Bitcoin({ price$ }) {
    // Redux hooks
    const dispatch = useDispatch()
    const { bitcoinStep, bitcoinStep$ } = useSelector((state) => state.settings)

    // Common handle hook for dispatching wallet-related actions
    function handleClick(action) {
        updateWallet(action, dispatch, bitcoinStep, bitcoinStep$)
    }

    return (
        <>
            <h1 className="Page__Subtitle">Bitcoin price is {price$}</h1>
            <div className="Buttons__Container">
                <button onClick={() => handleClick(increased)}>Increase bitcoin price (+{bitcoinStep$})</button>
                <button onClick={() => handleClick(decreased)}>Decrease bitcoin price (-{bitcoinStep$})</button>
            </div>
        </>
    )
}

Bitcoin.defaultProps = {
    price$: '',
}

Bitcoin.propTypes = {
    price$: PropTypes.string,
}

export default Bitcoin
