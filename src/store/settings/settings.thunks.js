// Assuming wallet data comes from backend
// @send http get req using axios as example
// @dispatch data to the store
import { getSettingsError, getSettingsSuccess } from './settings.slice'
import {
    BITCOIN_INITIAL_PRICE,
    BITCOIN_STEP,
    HIGH_PRICE,
    INIT_FIAT,
    LOW_PRICE,
    MIN_BUY_BITCOIN,
    MAX_DEPOSIT,
    MAX_WITHDRAWAL,
    USER_CURRENCY,
} from '../../config/app.config'

export function fetchAppSettings() {
    return async (dispatch) => {
        try {
            // const response = await http.get('/wallet')
            /* dispatch the data from request getWalletSuccess(response.data) */
            dispatch(
                getSettingsSuccess({
                    maxDeposit: MAX_DEPOSIT,
                    maxWithdrawal: MAX_WITHDRAWAL,
                    initFiat: INIT_FIAT,
                    highPrice: HIGH_PRICE,
                    lowPrice: LOW_PRICE,
                    minBuyBitcoin: MIN_BUY_BITCOIN,
                    bitcoinStep: BITCOIN_STEP,
                    bitcoinInitPrice: BITCOIN_INITIAL_PRICE,
                    userCurrency: USER_CURRENCY,
                }),
            )
            // return response
        } catch (e) {
            dispatch(getSettingsError())
            console.log(`GET request failed: ${e}`)
            throw e
        }
    }
}
