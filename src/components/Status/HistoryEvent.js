import React from 'react'
import PropTypes from 'prop-types'

/*
 * History event item
 */
function HistoryEvent({ item }) {
    const { timestamp, event } = item

    return (
        <div className="History__Event">
            <p>{timestamp}</p>
            <p>{event}</p>
        </div>
    )
}

HistoryEvent.propTypes = {
    item: PropTypes.object.isRequired,
}

export default HistoryEvent
