import initialState from '../initialState'
import { createSlice } from '@reduxjs/toolkit'
import format from '../../helpers/formatAmount'

const settingsSlice = createSlice({
    name: 'settings',
    initialState: initialState.settings,
    reducers: {
        getSettingsSuccess: (state, action) => {
            const { maxDeposit, maxWithdrawal, bitcoinStep, userCurrency } = action.payload
            return {
                ...action.payload,
                maxDeposit$: format(maxDeposit, userCurrency),
                maxWithdrawal$: format(maxWithdrawal, userCurrency),
                bitcoinStep$: format(bitcoinStep, userCurrency),
            }
        },
        getSettingsError: (state) => state,
    },
})

export const { getSettingsSuccess, getSettingsError } = settingsSlice.actions

export default settingsSlice.reducer
