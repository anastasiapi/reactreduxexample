/* eslint-disable */
import React, { useEffect } from 'react'
import Header from '../Header/Header'
import Navigation from '../Navigation/Navigation'
import Status from '../Status/Status'
import './app.scss'
import { Redirect, Route, Switch } from 'react-router-dom'
import Bitcoin from '../../screens/Bitcoin/Bitcoin'
import { useDispatch, useSelector } from 'react-redux'
import { fetchWalletData } from '../../store/wallet/wallet.thunks'
import { fetchBitcoinPrice } from '../../store/bitcoin/bitcoin.thunks'
import { fetchAppSettings } from '../../store/settings/settings.thunks'
import { routes } from '../../config/app.config'
import LeftSidebar from '../Sidebar/LeftSidebar'
import RightSidebar from '../Sidebar/RightSidebar'

function App() {
    // Redux hooks
    const dispatch = useDispatch()
    const { price, price$ } = useSelector((state) => state.bitcoin)
    const { highPrice, minBuyBitcoin } = useSelector((state) => state.settings)

    // Common props for children components
    const title = minBuyBitcoin === 1 ? 'Bitcoin' : 'Bitcoins'
    const isHigh = price >= highPrice

    const redirect = (path) => (path === '/' ? '/wallet' : undefined)

    // Initial data getters of common data on app load
    useEffect(() => {
        dispatch(fetchWalletData())
        dispatch(fetchBitcoinPrice())
        dispatch(fetchAppSettings())
    }, [])

    return (
        <div className="App">
            <Header />

            <div className="App__Container">
                <LeftSidebar />
                <main className="Main__Container">
                    <Switch>
                        {routes.map((route) => (
                            <Route
                                path={`${route.path}`}
                                key={route.id}
                                component={() => (
                                    <route.component
                                        bitcoinPrice={price}
                                        price$={price$}
                                        isHigh={isHigh}
                                        title={title}
                                        to={redirect(route.path)}
                                    />
                                )}
                            />
                        ))}
                    </Switch>
                </main>
                <RightSidebar />
            </div>
        </div>
    )
}

export default App
