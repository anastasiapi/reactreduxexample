import Wallet from '../screens/Wallet/Wallet'
import Buy from '../screens/Buy/Buy'
import Sell from '../screens/Sell/Sell'
import Bitcoin from '../screens/Bitcoin/Bitcoin'
import NotFound from '../screens/NotFound/NotFound'
import { Redirect } from 'react-router-dom'

export const APP_NAME = 'Bitcoin Frenzy'

/* This would come from backend on initial load */
export const MAX_DEPOSIT = 100
export const MAX_WITHDRAWAL = 100
export const INIT_FIAT = 200
export const MIN_BUY_BITCOIN = 1
export const HIGH_PRICE = 10000
export const LOW_PRICE = 10000
export const BITCOIN_STEP = 1000
export const BITCOIN_INITIAL_PRICE = 1000
export const USER_CURRENCY = 'usd'

/* List of available events */
export const DEPOSIT = 'wallet/deposit'
export const WITHDRAW = 'wallet/withdraw'
export const PURCHASED = 'wallet/purchased'
export const SOLD = 'wallet/sold'
export const INCREASED = 'bitcoin/increased'
export const DECREASED = 'bitcoin/decreased'

/* Routes list */
export const routes = [
    { path: '/wallet', label: 'MY WALLET', id: 0, component: Wallet },
    { path: '/buy', label: 'BUY BITCOIN', id: 1, component: Buy },
    { path: '/sell', label: 'SELL BITCOIN', id: 2, component: Sell },
    { path: '/bitcoin', label: 'BITCOIN PRICE', id: 3, component: Bitcoin },
    { path: '/', label: 'HOME', id: 4, component: Redirect, exclude: true },
    { path: '*', label: '404', id: 5, component: NotFound, exclude: true },
]
