import wallet from './wallet/wallet.slice'
import bitcoin from './bitcoin/bitcoin.slice'
import settings from './settings/settings.slice'
import history from './history/history.slice'

export default {
    wallet,
    bitcoin,
    settings,
    history,
}
