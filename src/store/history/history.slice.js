import initialState from '../initialState'
import { createSlice } from '@reduxjs/toolkit'
import { DECREASED, DEPOSIT, INCREASED, PURCHASED, SOLD, WITHDRAW } from '../../config/app.config'

const historySlice = createSlice({
    name: 'history',
    initialState: initialState.history,
    reducers: {
        updateHistory: (state, action) => [
            { id: setID(state), timestamp: setTime(), event: mapTypes(action.payload) },
            ...state,
        ],
    },
})

// Set unique ID for each event in history list
function setID(list) {
    return list.length === 0
        ? 0
        : list.reduce((acc, current) => {
        return Math.max(acc, current.id)
    }, 0) + 1
}

// Set time of event in history list
function setTime() {
    const datetime = new Date()

    return datetime.toLocaleString()
}

// List of available events
const mapTypes = (data) =>
    ({
        [DEPOSIT]: `${data.amount} Deposit`,
        [WITHDRAW]: `${data.amount} Withdrawal`,
        [PURCHASED]: `Purchased ${data.amount} Bitcoin`,
        [SOLD]: `Sold ${data.amount} Bitcoin`,
        [INCREASED]: `Increased Bitcoin price by ${data.amount}`,
        [DECREASED]: `Decreased Bitcoin price by ${data.amount}`,
    }[data.type])

export const { updateHistory } = historySlice.actions

export default historySlice.reducer
